/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.dao;

import beans.model.Support;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author Dawid
 */
@Remote
public interface SupportFacadeRemote {

    void create(Support support);

    void edit(Support support);

    void remove(Support support);

    Support find(Object id);

    List<Support> findAll();

    List<Support> findRange(int[] range);

    int count();

    List<Support> findSupportByNickAndPassword(String nick, String password);

    void removeSupport(Support support);
    
}
