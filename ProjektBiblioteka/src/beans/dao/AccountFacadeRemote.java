/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.dao;

import beans.model.Account;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author Dawid
 */
@Remote
public interface AccountFacadeRemote {

    /**
     * Method for create new Account in Database
     * @param account
     */
    void create(Account account);

    /**
     * DO NOT USE THIS, PROBABLY DOSN'T WORK
     * @param account
     */
    void edit(Account account);

    /**
     * Don't use this metod
     * @param account
     * @see removeAccount(Account account)
     */
    void remove(Account account);

    /**
     * Method find Account by id (String nickAccount)
     * @param id 
     * @return
     */
    Account find(Object id);

    /**
     * Method return all Account in Database
     * @return
     */
    List<Account> findAll();

    /**
     * Method return Account from Database by range
     * @param range
     * @return
     */
    List<Account> findRange(int[] range);

    /**
     * I don't have idea, what this function doing.
     * @return
     */
    int count();

    /**
     * Method return all mails found in AccountDatabase
     * @return
     */
    List<String> getAllMails();

    /**
     * Method set mail in Account
     * @param account
     * @param mail
     */
    void setMail(Account account, String mail);

    /**
     * Method return Account found by Nick and Password.
     * If Account don't exist, return empty list
     * @param nick
     * @param password
     * @return
     */
    List<Account> findAccountByNickAndPassword(String nick, String password);

    /**
     * Method remove Account from Database
     * @param account
     */
    void removeAccount(Account account);
    
}
