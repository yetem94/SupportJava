/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.dao;

import beans.model.Account;
import beans.model.Post;
import beans.model.Support;
import java.util.Date;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author Dawid
 */
@Remote
public interface PostFacadeRemote {

    /**
     * Method for create new Post in Database
     *
     * @param post
     */
    void create(Post post);

    /**
     * DO NOT USE THIS, PROBABLY DOSN'T WORK
     *
     * @param post
     */
    void edit(Post post);

    /**
     * Don't use this metod
     *
     * @param post
     * @see removePost(Post post)
     */
    void remove(Post post);

    /**
     * Method find Post by id (int id)
     *
     * @param id
     * @return
     */
    Post find(Object id);

    /**
     *
     * @return all Post in Database
     */
    List<Post> findAll();

    /**
     * Method return Post from Database by range
     *
     * @param range
     * @return
     */
    List<Post> findRange(int[] range);

    /**
     * I don't have idea, what this function doing.
     *
     * @return
     */
    int count();

    /**
     * Method return List Post by Account
     *
     * @param account
     * @return
     */
    List<Post> getPostByAccount(Account account);

    /**
     *
     * @return max Id Post in Database (to create new)
     */
    List<Post> getLastId();

    /**
     * Method remove Post in Database
     *
     * @param post
     */
    void removePost(Post post);

    /**
     * Method set Support in Post
     *
     * @param support
     * @param post
     */
    void setSupport(Support support, Post post);

    /**
     * Method find Post by Title
     *
     * @param title
     * @return List of Post
     */
    List<Post> findPostByTitle(String title);

    void addRepost(Post post, Support support, String repostInfo, Date date);

    List<Post> findNoRepostPost();
}
