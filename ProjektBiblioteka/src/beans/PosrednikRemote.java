/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import beans.model.Account;
import beans.model.Post;
import beans.model.Support;
import java.util.Date;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author Marcin
 */
@Remote
public interface PosrednikRemote {

//-------------Other Methods------------//
    /**
     * Method send email with subject and body to mail
     *
     * @param mail
     * @param subject
     * @param body
     */
    void send(String mail, String subject, String body);

//-------------Getters------------//
    /**
     * Method for get all Account from Database
     *
     * @return List of all Account
     */
    List<Account> getAccount();

    /**
     * Method for get all Account mails from Databes
     *
     * @return All Mails (String) from Database
     */
    List<String> getAllMails();

    /**
     * Method for get all Support from Database
     *
     * @return List of all Supports
     */
    List<Support> getSupports();

    /**
     * Method for get all Post from Database
     *
     * @return List of all Posts
     */
    List<Post> GetPosts();

//-------------Adders------------//
    /**
     * Method add new Post to Database fixed id, you don't need initialize them
     *
     * @param post
     */
    void addPost(Post post);

    /**
     * Method add new Account to Database
     *
     * @param account
     */
    void addAccount(Account account);

    /**
     * Method for get all Post added by Account
     *
     * @param account
     * @return List of Posts belonging to Account
     */
    List<Post> getPostByAccount(Account account);

    /**
     * Method to edit email to Account in Database
     *
     * @param account
     * @param mail
     */
    void setMail(Account account, String mail);

    /**
     * Method check Account exist in Database
     *
     * @param account
     * @param password
     * @return True if Account/Support exist and false otherwise
     */
    int isAccount(String account, String password);

    /**
     * Method get Account find in Database by nick and password
     *
     * @param nick
     * @param password
     * @return found Account
     */
    Account findAccountByNickAndPassword(String nick, String password);

    /**
     * Method get Support find in Database by nick and password
     *
     * @param nick
     * @param password
     * @return found Support
     */
    Support findSupportByNickAndPassword(String nick, String password);

    /**
     * Method for get last ID for create new Post
     *
     * @return Integer with max id from Post
     */
    int getLastIdPost();

    /**
     * Method to remove Account from Database. Remove all Post related with
     * Account
     *
     * @param account
     */
    void removeAccount(Account account);

    /**
     * Method to remove Support from Database. Remove link to Support in all
     * related Post, but doesn't remove Post
     *
     * @param support
     */
    void removeSupport(Support support);

    /**
     * Method to remove Post from Database
     *
     * @param post
     */
    void removePost(Post post);

    /**
     * Method find Post by Title
     *
     * @param title
     * @return Post
     */
    Post findPostByTitle(String title);

    /**
     * Method update Post by set Support, repostInfo and repostDate. Method add
     * repost (and can edit repost)
     *
     * @param post
     * @param support
     * @param repostInfo
     * @param date
     */
    void addRepost(Post post, Support support, String repostInfo, Date date);

    List<Post> findNoRepostPost();
}
