/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Dawid
 */
@Entity
@Table(name = "SUPPORT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Support.findAll", query = "SELECT s FROM Support s"),
    @NamedQuery(name = "Support.findByNickSupport", query = "SELECT s FROM Support s WHERE s.nickSupport = :nickSupport"),
    @NamedQuery(name = "Support.findByPassword", query = "SELECT s FROM Support s WHERE s.password = :password"),
    @NamedQuery(name = "Support.findByEmail", query = "SELECT s FROM Support s WHERE s.email = :email")})
public class Support implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "NICK_SUPPORT")
    private String nickSupport;
    @Size(max = 10)
    @Column(name = "PASSWORD")
    private String password;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 30)
    @Column(name = "EMAIL")
    private String email;
    @OneToMany(mappedBy = "nickSupport")
    private List<Post> postList;

    public Support() {
    }

    public Support(String nickSupport) {
        this.nickSupport = nickSupport;
    }

    public String getNickSupport() {
        return nickSupport;
    }

    public void setNickSupport(String nickSupport) {
        this.nickSupport = nickSupport;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlTransient
    public List<Post> getPostList() {
        return postList;
    }

    public void setPostList(List<Post> postList) {
        this.postList = postList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nickSupport != null ? nickSupport.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Support)) {
            return false;
        }
        Support other = (Support) object;
        if ((this.nickSupport == null && other.nickSupport != null) || (this.nickSupport != null && !this.nickSupport.equals(other.nickSupport))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.model.Support[ nickSupport=" + nickSupport + " ]";
    }
    
}
