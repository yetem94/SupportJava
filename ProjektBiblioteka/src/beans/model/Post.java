/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dawid
 */
@Entity
@Table(name = "POST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Post.findAll", query = "SELECT p FROM Post p"),
    @NamedQuery(name = "Post.findById", query = "SELECT p FROM Post p WHERE p.id = :id"),
    @NamedQuery(name = "Post.findByPostDate", query = "SELECT p FROM Post p WHERE p.postDate = :postDate"),
    @NamedQuery(name = "Post.findByPostInfo", query = "SELECT p FROM Post p WHERE p.postInfo = :postInfo"),
    @NamedQuery(name = "Post.findByRepostDate", query = "SELECT p FROM Post p WHERE p.repostDate = :repostDate"),
    @NamedQuery(name = "Post.findByRepostInfo", query = "SELECT p FROM Post p WHERE p.repostInfo = :repostInfo")})
public class Post implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "POST_DATE")
    @Temporal(TemporalType.DATE)
    private Date postDate;
    @Size(max = 255)
    @Column(name = "POST_INFO")
    private String postInfo;
    @Size(max = 100)
    @Column(name = "TITLE")
    private String title;
    @Column(name = "REPOST_DATE")
    @Temporal(TemporalType.DATE)
    private Date repostDate;
    @Size(max = 255)
    @Column(name = "REPOST_INFO")
    private String repostInfo;
    @JoinColumn(name = "NICK_ACCOUNT", referencedColumnName = "NICK_ACCOUNT")
    @ManyToOne
    private Account nickAccount;
    @JoinColumn(name = "NICK_SUPPORT", referencedColumnName = "NICK_SUPPORT")
    @ManyToOne
    private Support nickSupport;

    public Post() {
    }

    public Post(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getPostInfo() {
        return postInfo;
    }

    public void setPostInfo(String postInfo) {
        this.postInfo = postInfo;
    }

    public Date getRepostDate() {
        return repostDate;
    }

    public void setRepostDate(Date repostDate) {
        this.repostDate = repostDate;
    }

    public String getRepostInfo() {
        return repostInfo;
    }

    public void setRepostInfo(String repostInfo) {
        this.repostInfo = repostInfo;
    }

    public Account getAccount() {
        return nickAccount;
    }

    public void setAccount(Account nickAccount) {
        this.nickAccount = nickAccount;
    }

    public Support getSupport() {
        return nickSupport;
    }

    public void setSupport(Support nickSupport) {
        this.nickSupport = nickSupport;
    }
    
    public boolean isRepost(){
        return this.repostInfo!=null;
    }
    
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Post)) {
            return false;
        }
        Post other = (Post) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.model.Post[ id=" + id + " ]";
    }
    
}
