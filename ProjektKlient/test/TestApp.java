/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import beans.PosrednikRemote;
import beans.model.Account;
import beans.model.Post;
import beans.model.Support;
import static java.awt.image.ImageObserver.WIDTH;
import java.sql.SQLClientInfoException;
import java.sql.SQLDataException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.internal.runners.statements.ExpectException;
import static window.OknoGlowne.CreateAccount;
import java.lang.NullPointerException;
import java.sql.Date;
import java.time.LocalDate;

/**
 *
 * @author Kacpes
 */
public class TestApp {
    
    public TestApp() {}
    
    String user = "asd";
    String pass = "qwe";

    String wrongUser = "NieWiemCoJaRobieTu";
    String wrongPass = "pqs";
        
    PosrednikRemote ejb;
    InitialContext ic;
    
    @BeforeClass
    public static void setUpClass() {}
    
    @AfterClass
    public static void tearDownClass() {}
    

    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {}
    

    
    //ejb
    @Test (expected = NullPointerException.class)
    public void noAccountTest() throws NamingException
    {
       
        ejb = (PosrednikRemote) ic.lookup("java:global/ProjektSerwer/ProjektSerwer-ejb/Posrednik!beans.PosrednikRemote");
        
        int wynik =0;
        wynik = ejb.isAccount(wrongUser, wrongPass);
        Assert.assertEquals(0, wynik);
    }
 
    @Test (expected = NullPointerException.class)
    public void isAccountTest()
    {
        
        int wynik=0;
        wynik = ejb.isAccount(user, pass);
        Assert.assertEquals(1,wynik);
    }
   
    //acc
    @Test (expected = NullPointerException.class)//(expected = SQLException.class)
    public void createAccAcception ()
    {
        Account ac = new Account();
        String mail = "exeption@no.mail";
        String nick = user;
        String password = pass;
        ejb.addAccount(ac);
        ejb.addAccount(ac);
    }
  
    @Test //(expected = NullPointerException.class)
    public void accountHash()
    {
        Account acc = new Account();
        acc.setNickAccount(user);
        Assert.assertNotSame(acc.hashCode(), acc);
    }
   
    @Test
    public void accountNullHash()
    {
        Account acc = new Account();
        Assert.assertEquals(acc.hashCode(), 0);
    }

    @Test
    public void accountEquals()
    {
        Account acc = new Account();
        Account acp = new Account();
        Assert.assertEquals(acc.equals(acp),true);
    }

    @Test
    public void accountNotEquals()
    {
        String email = "to@nie.jest";
        Account acc = new Account();
        Account acp = new Account();
        acc.setEmail(email);
        acc.setNickAccount(user);
        acc.setPassword(pass);    
        Assert.assertEquals(acc.equals(acp),false);
    }
    
    @Test
    public void accountToString()
    {
        Account acc = new Account();
        acc.setNickAccount(user);
        Assert.assertNotSame(acc.toString(), acc.getNickAccount());
    }

    @Test
    public void setPostList()
    {
        Account acc = new Account();
        Assert.assertEquals(acc.getPostList(), null);
    }
    
    @Test
    public void getPostList()
    {
        List<Post> lista;
        lista = new ArrayList<Post>();
        Account acc = new Account();
        acc.setPostList(lista);
        Assert.assertEquals(acc.getPostList(), lista);
    }
    
    @Test
    public void setEmail()
    {
        String email = "to@nie.jest";
        Account acc = new Account();
        acc.setEmail(email);
        Assert.assertEquals(acc.getEmail(), email);
    }
    
    @Test
    public void getEmail()
    {
        Account acc = new Account();
        Assert.assertEquals(acc.getEmail(), null);
    }
    
    @Test
    public void setPass()
    {
        Account acc = new Account();
        acc.setPassword(pass);
        Assert.assertEquals(acc.getPassword(), pass);
    }
   
    @Test
    public void getPass()
    {
        Account acc = new Account();
        Assert.assertEquals(acc.getPassword(), null);
    }
    
    @Test
    public void setAccName()
    {
        Account acc = new Account();
        acc.setNickAccount(user);
        Assert.assertEquals(acc.getNickAccount(), user);
    }
   
    @Test
    public void getAccName()
    {
        Account acc = new Account();
        Assert.assertEquals(acc.getNickAccount(), null);
    }
    
    @Test
    public void getAccNameFromConstructor()
    {
        Account acc = new Account(user);
        Assert.assertEquals(acc.getNickAccount(), user);
    }
//25    
//Post
    @Test //(expected = NullPointerException.class)
    public void getIdTest()
    {
         Post p = new Post();
         Assert.assertEquals(null, p.getId() );
    }
    
    @Test //(expected = NullPointerException.class)
    public void getIdConstructorTest()
    {
        Integer i = 10;
        Post p = new Post(i);
         Assert.assertEquals(i, p.getId() );
    }
    
    
    @Test
    public void setIdTest()
    {
        Integer i = 10;
        Post p = new Post();
        p.setId(i);
        Assert.assertEquals(p.getId(), i);
    }
    
    @Test (expected = NullPointerException.class)
    public void GetNullTitleTest()
    {
        Post p = null;
        Assert.assertEquals(true, p.getTitle());
        
    }
    
    @Test
    public void setTitleTest()
    {
        String title = "To jest tytuł";
        Post p = new Post();
        p.setTitle(title);
        Assert.assertEquals(title, p.getTitle());
    }
    
    @Test
    public void GetTitleTest()
    {
        Post p = new Post();
        Assert.assertEquals(null, p.getTitle());
    }

    @Test
    public void toStringPostTest()
    {
        Post p = new Post();
        Integer id = 35;
        p.setId(id);
        Assert.assertNotSame(id, p.toString());
    }
    
    @Test
    public void postHash()
    {
        Post p = new Post();
        Integer i=10;
        p.setId(i);
        Assert.assertNotSame(p.hashCode(), p.getId().toString());
    }

    @Test
    public void postNullHash()
    {
        Post p = new Post();
        Assert.assertEquals(p.hashCode(), 0);
    }

    @Test
    public void postEquals()
    {
        Post p = new Post();
        Post k = new Post();
        Assert.assertEquals(p.equals(k),true);
    }
    //28
    
    @Test
    public void postHashcode()
    {
        Integer i =10;
        Post p = new Post(i);
        Assert.assertNotSame(p.hashCode(), p.getId().toString());
    }
    
    @Test
    public void postToString()
    {
        Integer i =10;
        Post p = new Post(i);
        Assert.assertNotSame(p.toString(),p.getId().toString());
    }
    
    @Test
    public void postGetNullSupport()
    {
        Post p = new Post();
        Assert.assertEquals(p.getSupport(), null);
    }
    
    @Test
    public void postSetSupport()
    {
        Support s = new Support(user);
        Post p = new Post();
        p.setSupport(s);
        Assert.assertEquals(p.getSupport(), s);
    }
    
    @Test
    public void postSetRepostInfo()
    {
        Post p = new Post();
        String rep = "Repost Test";
        p.setRepostInfo(rep);
        Assert.assertEquals(p.getRepostInfo(), rep);
    }
    
    @Test
    public void postGetRepostInfo()
    {
        Post p = new Post();
        Assert.assertEquals(p.getPostInfo(), null);
    }
    
    @Test
    public void postTrueTestRepostInfo()
    {
        Post p = new Post();
        String rep = "Repost Test";
        p.setRepostInfo(rep);
        Assert.assertEquals(p.isRepost(),true);
    }
    
    @Test
    public void postFalseTestRepostInfo()
    {
        Post p = new Post();
        Assert.assertEquals(p.isRepost(),false);
    }
    
    @Test
    public void postGetNullAccount()
    {
        Post p = new Post();
        Assert.assertEquals(p.getAccount(), null);
    }
    
    @Test
    public void postSetAccount()
    {
        Post p = new Post();
        Account acc = new Account();
        p.setAccount(acc);
        Assert.assertEquals(p.getAccount(), acc);
    }
    
    @Test
    public void posrSetRepostDate()
    {
        Post p = new Post();
        Date d = Date.valueOf(LocalDate.MIN);
        p.setRepostDate(d);
        Assert.assertEquals(p.getRepostDate(), d);
    }
    
    @Test
    public void postGetNullRepostDate()
    {
        Post p = new Post();
        Assert.assertEquals(p.getRepostDate(), null);
    }
    
    @Test
    public void postSetPostDate()
    {
        Post p = new Post();
        Date d = Date.valueOf(LocalDate.MIN);
        p.setPostDate(d);
        Assert.assertEquals(p.getPostDate(), d);
    }
    
    @Test
    public void postGetNullPostDate()
    {
        Post p = new Post();
        Assert.assertEquals(p.getPostDate(), null);
    }
    
    @Test
    public void postSetPostInfo()
    {
        Post p = new Post();
        String rep = "Post Test";
        p.setPostInfo(rep);
        Assert.assertEquals(rep, p.getPostInfo());
    }
    
    @Test
    public void postGetNullPostInfo()
    {
        Post p = new Post();
        Assert.assertEquals(null, p.getPostInfo());
    }
    
    
}
