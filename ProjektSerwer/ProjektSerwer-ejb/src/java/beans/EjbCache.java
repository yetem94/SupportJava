/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import beans.model.Post;
import beans.model.Support;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Schedule;
import javax.mail.MessagingException;
import javax.naming.NamingException;

/**
 *
 * @author Marcin
 */
@Singleton
public class EjbCache {
    @EJB
    public PosrednikRemote posrednik;

    private int a;
    
    
    @Schedule(hour = "*", minute = "*", second = "*")
    void clearCache(){
        System.out.println( (a=(a+1)%60));
    }

    @Schedule(hour = "*",minute = "*", second = "0")
    void sendMailtoAdmins() throws NamingException, MessagingException{
        System.out.println("=-Funkcja przypominajki dziala -=");
        System.out.println("=="+posrednik.findNoRepostPost().size()+"==");
        String body = "Witaj!\n\n Zostało jeszcze "+posrednik.findNoRepostPost().size()+" zgłoszeń do rozpatrzenia."
                + "Sprawdz je w najblizszym czasie";
        for(Support s:posrednik.getSupports()){
            posrednik.send( s.getEmail(), "Statystyka postów", body);
            System.out.println("Wyslalem mail do "+s.getEmail());
        }
    }
    
    @Schedule
    void clearPost(){
   
        Date d = new Date();
        for(Post p:posrednik.GetPosts()){
            if(p.getPostInfo()!=null){
                if(p.getPostDate().getMonth()+2<d.getMonth()){
                    posrednik.removePost(p);
                }
            }
        }
    }
   
    
}
