/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.dao;

import beans.model.Account;
import beans.model.Post;
import beans.model.Support;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Dawid
 */
@Stateless
public class PostFacade extends AbstractFacade<Post> implements beans.dao.PostFacadeRemote {

    @PersistenceContext(unitName = "ProjektSerwer-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PostFacade() {
        super(Post.class);
    }

    @Override
    public List<Post> getPostByAccount(Account account) {
        String SQL = "SELECT a FROM Post a WHERE a.nickAccount=?1";
        EntityManager em = getEntityManager();
        Query query = em.createQuery(SQL);
        query.setParameter(1, account);
        List list = query.getResultList();
        return list;

    }

    @Override
    public List<Post> getLastId() {
        String SQL = "SELECT a FROM Post a WHERE a.id=(SELECT max(b.id) FROM Post b)";
        EntityManager em = getEntityManager();
        Query query = em.createQuery(SQL);
        List list = query.getResultList();
        return list;
    }

    @Override
    public void removePost(Post post) {
        String SQL = "DELETE FROM Post a WHERE a=?1";
        EntityManager em = getEntityManager();
        em.merge(post);
        
        Query query = em.createQuery(SQL);
        query.setParameter(1, post);
        
        query.executeUpdate();
        
    }

    @Override
    public void setSupport(Support support, Post post) {
       String SQL = "UPDATE Post SET nickSupport = ?1 WHERE id=?2";
        EntityManager em = getEntityManager();
        em.merge(post);
        
        Query query = em.createQuery(SQL);
        query.setParameter(1, support);
        query.setParameter(2, post.getId());
        
        query.executeUpdate(); 
        
    }

    @Override
    public List<Post> findPostByTitle(String title) {
        String SQL = "SELECT a FROM Post a WHERE a.title=?1";
        EntityManager em = getEntityManager();
        Query query = em.createQuery(SQL);
        query.setParameter(1, title);
        List list = query.getResultList();
        return list;
    }

    @Override
    public void addRepost(Post post, Support support, String repostInfo, Date date) {
         String SQL = "UPDATE Post SET nickSupport = ?1, repostInfo = ?2, repostDate = ?3 WHERE id=?4";
        EntityManager em = getEntityManager();
        em.merge(post);
        
        Query query = em.createQuery(SQL);
        query.setParameter(1, support);
        query.setParameter(2, repostInfo);
        query.setParameter(3, date);
        query.setParameter(4, post.getId());
        
        query.executeUpdate(); 
    }

    @Override
    public List<Post> findNoRepostPost() {
        String SQL = "SELECT a FROM Post a WHERE a.repostInfo=null";
        EntityManager em = getEntityManager();
        Query query = em.createQuery(SQL);
        List list = query.getResultList();
        return list;
    }

    
    
}
