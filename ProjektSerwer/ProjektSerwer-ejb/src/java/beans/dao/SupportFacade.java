/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.dao;

import beans.model.Support;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Dawid
 */
@Stateless
public class SupportFacade extends AbstractFacade<Support> implements beans.dao.SupportFacadeRemote {
    @PersistenceContext(unitName = "ProjektSerwer-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SupportFacade() {
        super(Support.class);
    }

    @Override
    public List<Support> findSupportByNickAndPassword(String nick, String password) {
        String SQL = "SELECT a FROM Support a WHERE a.nickSupport=?1 AND a.password=?2";
        EntityManager em = getEntityManager();
        Query query = em.createQuery(SQL);
        query.setParameter(1, nick);
        query.setParameter(2, password);
        List list = null;
        list = query.getResultList();
        return list; 
    }

    @Override
    public void removeSupport(Support support) {
        String SQL = "DELETE FROM Support a WHERE a=?1";
        EntityManager em = getEntityManager();
        em.merge(support);
        
        Query query = em.createQuery(SQL);
        query.setParameter(1, support);
        
        query.executeUpdate();
        
    }
    
    
    
}
