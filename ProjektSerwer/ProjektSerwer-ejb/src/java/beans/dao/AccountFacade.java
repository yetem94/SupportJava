/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.dao;

import beans.model.Account;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Dawid
 */
@Stateless
public class AccountFacade extends AbstractFacade<Account> implements AccountFacadeRemote {
    @PersistenceContext(unitName = "ProjektSerwer-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    
    
    public AccountFacade() {
        super(Account.class);
    }

    @Override
    public List<String> getAllMails() {
        String SQL = "SELECT a.email FROM Account a";
        EntityManager em = getEntityManager();
        Query query = em.createQuery(SQL);
        
        List list = query.getResultList();
        return list;  
    }

    @Override
    public void setMail(Account account, String mail) {     
        String SQL = "UPDATE ACCOUNT a SET a.email = ?1 WHERE a=?2";
        EntityManager em = getEntityManager();
        Query q = em.createQuery(SQL);
        q.setParameter(1, mail);
        q.setParameter(2, account);
        em.getTransaction().begin();
        q.executeUpdate();   
        em.getTransaction().commit();
    }

    @Override
    public List<Account> findAccountByNickAndPassword(String nick, String password) {
        String SQL = "SELECT a FROM Account a WHERE a.nickAccount=?1 AND a.password=?2";
        EntityManager em = getEntityManager();
        Query query = em.createQuery(SQL);
        query.setParameter(1, nick);
        query.setParameter(2, password);
        List list  = null;
        list = query.getResultList();
        return list; 
    }

    @Override
    public void removeAccount(Account account) {
        String SQL = "DELETE FROM Account a WHERE a=?1";
        EntityManager em = getEntityManager();
        em.merge(account);
        
        Query query = em.createQuery(SQL);
        query.setParameter(1, account);
        
        query.executeUpdate();
        
    }
    
    
    
    
}
