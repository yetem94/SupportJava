/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import beans.dao.AccountFacadeRemote;
import beans.dao.PostFacadeRemote;
import beans.dao.SupportFacadeRemote;
import beans.model.Account;
import beans.model.Post;
import beans.model.Support;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.NamingException;

/**
 *
 * @author Marcin
 */
@Stateless
public class Posrednik implements PosrednikRemote {
//sprivate static Logger log = Logger.getLogger(Posrednik.class);
//-------------Inject EJB------------//
    @EJB
    private SupportFacadeRemote supportFacade;
    @EJB
    
    private PostFacadeRemote postFacade;
    @EJB
    private AccountFacadeRemote accountFacade;
    @EJB
    private EjbCache ejbCache;

    //-------------Send Email------------//
    @Override

    public void send(String mail, String subject, String body) {
        try {
            System.out.println("Wysylam maila na adres: " + mail);
            sendMail(mail, subject, body);
        } catch (NamingException ex) {
            System.out.println("blad z Naming");
        } catch (MessagingException ex) {
            System.out.println("qblad z Mailem");


        }

    }

    @Asynchronous
    public void sendMail(String email, String subject, String body) throws NamingException, MessagingException {
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", "poczta.o2.pl");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.ssl.enable", "true");
        Session session = Session.getInstance(props);
        MimeMessage m = new MimeMessage(session);

        try {
            InternetAddress[] address = {new InternetAddress(email)};
            m.setFrom(new InternetAddress("yetem@o2.pl"));
            m.setRecipients(Message.RecipientType.TO, address);
            m.setSubject(subject);
            m.setSentDate(new Date());
            m.setText(body);
            Transport.send(m, "yetem", "yetem94");
        } catch (MessagingException e) {
            System.out.println("zjebales kurwo kutasie");
        }
        
    }

//-------------Getters------------//

    
        @Override
    public List<Account> getAccount() {
        return accountFacade.findAll();
    }

    @Override
    public List<String> getAllMails() {
        return accountFacade.getAllMails();
    }

    @Override
    public List<Support> getSupports() {
        return supportFacade.findAll();
    }

    @Override
    public List<Post> GetPosts() {
        return postFacade.findAll();
    }

    @Override
    public List<Post> getPostByAccount(Account account) {
        return postFacade.getPostByAccount(account);
    }

//-------------Adders------------//
    @Override
    public void addAccount(Account account) {
        accountFacade.create(account);
    }

    @Override
    public void addPost(Post post) {
       
        post.setId(getLastIdPost()+1);
        postFacade.create(post);
    }

    @Override
    public void setMail(Account account, String mail) {
    accountFacade.setMail(account, mail);
    }
//-------------Finders------------//

    @Override
    public int isAccount(String nick, String password) {
        List<Account> list = accountFacade.findAccountByNickAndPassword(nick, password);
        if(list.isEmpty()){
            List<Support> list2 = supportFacade.findSupportByNickAndPassword(nick, password);
            if(list2.isEmpty()){
                return 0;
            }else return 2;
        }else return 1;
    }

    @Override
    public Account findAccountByNickAndPassword(String nick, String password) {
        return accountFacade.findAccountByNickAndPassword(nick, password).get(0);
    }

    @Override
    public Support findSupportByNickAndPassword(String nick, String password) {
        return supportFacade.findSupportByNickAndPassword(nick, password).get(0);
    }

    @Override
    public int getLastIdPost() {
        
        List<Post> list = postFacade.getLastId();
        if(list.isEmpty()){
            return 0;
        }else{
            return list.get(0).getId();
        }       
    }
    
  //-------------Removers------------//  

    @Override
    public void removeAccount(Account account) {
        List<Post> posts = postFacade.findAll();
        for(Post i:posts){
            if(i.getAccount().equals(account)) postFacade.removePost(i);
        }
        accountFacade.removeAccount(account);
    }

    @Override
    public void removeSupport(Support support) {
        List<Post> posts = postFacade.findAll();
        for(Post i:posts){
            if(i.getSupport().equals(support)) postFacade.setSupport(null, i);
        }
        supportFacade.removeSupport(support);
    }

    @Override
    public void removePost(Post post) {
        postFacade.removePost(post);
    }

    @Override
    public Post findPostByTitle(String title) {
        return postFacade.findPostByTitle(title).get(0);
    }

    @Override
    public void addRepost(Post post, Support support, String repostInfo, Date date) {
        postFacade.addRepost(post, support, repostInfo, date);
    }

    @Override
    public List<Post> findNoRepostPost() {
        return postFacade.findNoRepostPost();
    }
    
    
    

    
    
}
